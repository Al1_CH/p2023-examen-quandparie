﻿using QuandParie.Core.Domain;
using QuandParie.Core.Persistance;
using QuandParie.Core.ReadOnlyInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.Application
{
    public class BackOfficeAdministration
    {
        private readonly IOddsRepository oddsRepository;
        private readonly IWagerRepository wagerRepository;

        public BackOfficeAdministration(
            IOddsRepository oddsRepository,
            IWagerRepository wagerRepository)
        {
            this.oddsRepository = oddsRepository;
            this.wagerRepository = wagerRepository;
        }

        public async Task<IReadOnlyOdds> CreateOdds(string match, string condition, decimal value)
        {
            var odds = new Odds(match, condition, value);
            await oddsRepository.SaveAsync(odds);
            return odds;
        }

        public async Task ChangeOddsValue(Guid id, decimal value)
        {
            var odds = await oddsRepository.GetAsync(id);

            if (odds == null)
                throw new ArgumentException($"Id {id} does not match any odds", nameof(id));

            odds.Value = value;

            await oddsRepository.SaveAsync(odds);
        }

        public async Task<IReadOnlyOdds> GetOdds(Guid id)
            => await oddsRepository.GetAsync(id);

        public async Task<IReadOnlyList<IReadOnlyOdds>> GetOddsForMatch(string match)
            => await oddsRepository.GetForMatchAsync(match);

        public async Task ReportResult(Guid id, bool value)
        {
            var odds = await oddsRepository.GetAsync(id);

            if (odds == null)
                throw new ArgumentException($"Id {id} does not match any odds", nameof(id));

            if (odds.Outcome == value)
                throw new InvalidOperationException($"Cannot report twice an outcome for ${id}");

            odds.Outcome = value;
            await oddsRepository.SaveAsync(odds);

            var wagers = await wagerRepository.GetWagerHavingOdds(id);
            foreach (var wager in wagers)
            {
                if (wager.TryProcess())
                    await wagerRepository.SaveAsync(wager);
            }
        }
    }
    
}
