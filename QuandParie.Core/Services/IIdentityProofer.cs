﻿using QuandParie.Core.Domain;

namespace QuandParie.Core.Services
{
    public interface IIdentityProofer
    {
        bool Validates(Customer customer, byte[] identityProof);
    }
}